# VeryDeepSleep_BME280_sendToDB

Energy saving for ESP8266. [Project details here](https://arduinodiy.wordpress.com/2020/01/21/very-deepsleep-and-energy-saving-on-esp8266-part-2-sending-data-with-http/).

Reading a BME280, sending results to database with http

using Rui&Sara Santos' (randomnerdtutorials.com) database setup