<?php
 $con = mysqli_connect('localhost','USERNAME','YOURPW','esp-data');
?>
<!DOCTYPE HTML>
<html>
<head>
 <meta charset="utf-8">
 <title>Coldframe</title>
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript">
 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart);
 function drawChart() {
 var data = google.visualization.arrayToDataTable([

 ['Tijdstip','Temperatuur','Humidity'],
 <?php 
			$query = "SELECT * from SensorData WHERE id >=0";
			 $exec = mysqli_query($con,$query);
			 while($row = mysqli_fetch_array($exec)){
             echo "['".$row['reading_time']."',".$row['value1'].",".$row['value2']."],";
			 }
			 ?> 
 
 ]);

 var options = {
          title: 'ColdFrame',
          curveType: 'function',
          colors: ['#ff440e', '#0069ff'],
          pointSize: 5,
          
series: {
        0: { pointShape: { type: 'square'} },
        1: { pointShape: { type: 'circle'} },

},

          legend: { position: 'bottom' }
        };

 var chart = new google.visualization.LineChart(document.getElementById("columnchart12"));
 chart.draw(data,options);
 }
	
    </script>

</head>
<body>
 <div class="container-fluid">
 <div id="columnchart12" style="width: 100%; height: 500px;"></div>
 </div>

</body>
</html>
