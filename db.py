from flask import Flask, request, jsonify
import sqlite3

app = Flask(__name__)

# Database setup
DATABASE = 'sensor_data.db'

def init_db():
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS sensor_data (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            api_key TEXT NOT NULL,
            sensor TEXT NOT NULL,
            location TEXT NOT NULL,
            value1 REAL NOT NULL,
            value2 REAL NOT NULL,
            value3 REAL NOT NULL
        )
    ''')
    conn.commit()
    conn.close()

@app.route('/store_data', methods=['GET'])
def store_data():
    api_key = request.args.get('api_key')
    sensor = request.args.get('sensor')
    location = request.args.get('location')
    value1 = request.args.get('value1')
    value2 = request.args.get('value2')
    value3 = request.args.get('value3')

    # Validate parameters
    if not all([api_key, sensor, location, value1, value2, value3]):
        return jsonify({'error': 'Missing parameters'}), 400

    try:
        value1 = float(value1)
        value2 = float(value2)
        value3 = float(value3)
    except ValueError:
        return jsonify({'error': 'Invalid value for one of the values'}), 400

    # Insert into database
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()
    cursor.execute('''
        INSERT INTO sensor_data (api_key, sensor, location, value1, value2, value3)
        VALUES (?, ?, ?, ?, ?, ?)
    ''', (api_key, sensor, location, value1, value2, value3))
    conn.commit()
    conn.close()

    return jsonify({'status': 'success'}), 200

if __name__ == '__main__':
    init_db()
    app.run(debug=True)
